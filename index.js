#!/usr/bin/env node

const program = require('commander');
const parse = require('./parser');
const libraries = require('./lib/libraries');

program
  .description('Extract the toggles of a project')
  .arguments('<directory> <filepath> <library>')
  .option('--history', 'Extract across all commits of the project')
  .option('--break', 'Stop at first sign of toggles. Useful for exploratory runs')
  .option('--from <commit>', 'Start the extraction from the specified commit')
  .action((directory, filepath, library) => {
    const programOptions = {
      history: program.history,
      breakOnFirst: program.break,
      from: program.from,
    };
    parse(filepath, Object.assign({ cwd: directory, library }, programOptions))
      .then(toggles => process.stdout.write(JSON.stringify(toggles)))
      .catch((error) => {
        console.error(error);
        if (error.name === 'GitCheckoutError') {
          process.exit(110);
        }
      });
  });

program
  .command('libraries')
  .description('List all supported toggling libraries')
  .action(() => {
    console.log(Object.keys(libraries).map(lib => `${lib} (${libraries[lib].language})`).join('\n'));
  });

program.parse(process.argv);
