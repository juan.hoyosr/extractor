const languages = {
  JavaScript: 'JavaScript',
  Python: 'Python',
};

module.exports = {
  'feature-toggles': {
    name: 'feature-toggles',
    language: languages['JavaScript'],
    parser: 'nodejsIPC',
    command: 'extractor-js',
    args: ['ipc']
  },
  'django-waffle': {
    name: 'django-waffle',
    language: languages['Python'],
    parser: 'pythonIPC',
    command: 'extractor-python.py',
    args: ['ipc']
  }
};
