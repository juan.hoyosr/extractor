const { Transform } = require('stream');
const { exec } = require('child_process');
const debug = require('debug')('commit-parser');
const progressDebug = require('debug')('commit-parser:progress');
const git = require('../git');

class CommitParser extends Transform {
  constructor(options = {}) {
    options.highWaterMark = 1;
    super(Object.assign(options, { objectMode: true }));

    this.projectParser = options.projectParser;
    if (!this.projectParser) throw new Error('A project parser is required');
    
    this.cwd = options.cwd || '';
    this.parser = options.parser;
    this.breakOnFirst = options.breakOnFirst;

    // progress tracking
    this.totalFiles = options.totalFiles || 0;
    this.parsedFiles = 0;

    progressDebug(`Parsing ${this.totalFiles} files`);
  }

  get progress() {
    if (this.totalFiles <= 0) return 0;
    const progress = (this.parsedFiles / this.totalFiles) * 100
    return parseInt(progress * 10) / 10;
  }

  /*
  * We do not care about the order messages are returned by this.parser while
  * the returned toggles stay within the same commit. Gadly, _transform() is
  * guaranteed to never be called in parallel, thus every commit is transformed
  * sequentially.
  */
  _transform(commit, encoding, callback) {
    debug('Parsing %s', commit.commit);

    const filepaths = commit.files
      .filter(f => f.action !== 'D')
      .map(({ filepath }) => filepath);

    git.cleanup({ cwd: this.cwd, checkoutRef: commit.commit })
      .then(() => {
        const globPattern = filepaths.length === 1 ? filepaths[0] : `{${filepaths.join(',')}}`;
        return this.projectParser(globPattern, { cwd: this.cwd, parser: this.parser, breakOnFirst: this.breakOnFirst, commit });
      })
      .then((togglesSets) => {
        const toggles = togglesSets.filter((fileToggles) => {
          const error = fileToggles.__error__;
          if (error) {
            debug(`Parsing error: ${error.msg} (${error.filepath})`);
            const index = commit.files.findIndex(file => file.filepath === error.filepath);
            if (index > -1) {
              commit.files.splice(index, 1);
            } else {
              throw new Error('Cannot find a file in the commit');
            }
            return false;
          }

          return true;
        });

        this.parsedFiles += filepaths.length;
        progressDebug(`${commit.commit} (${this.progress}%)`)

        return Promise.resolve({
          commit,
          toggles,
        });
      })
      .then(snapshot => callback(null, snapshot))
      .catch(callback);
  }
}

module.exports = CommitParser;
