const libraries = require('./libraries');
const parsers = require('./parsers');

class ParserFactory {
  constructor() {
    this._parsers = [];
  }

  async build(libraryName) {
    const library = libraries[libraryName];
    const parser = parsers[`${library.parser}Parser`];
    return await parser(library);
  }
}

module.exports = new ParserFactory();
