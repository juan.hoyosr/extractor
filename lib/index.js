module.exports = {
  LogParser: require('./log-parser'),
  CommitParser: require('./commit-parser'),
  TogglesDiff: require('./toggles-diff'),
};
