const Parser = require('./parser');
const { PythonShell } = require('python-shell');

function pythonIPCParser(library) {
  const { command, args, name } = library;

  const parser = new Parser({
    launch() {
      const options = {
        // Using json mode will make python errors not to be parseable
        // so it makes it difficult to debug.
        // TODO: report & fix upstream
        mode: 'text',
        pythonPath: process.env.PYTHON_PATH,
        pythonOptions: ['-u'],
        scriptPath: process.env.SCRIPT_PATH,
        args,
      };
      const child = new PythonShell(command, options);

      // catch early spawn errors
      child.once('error', (err) => {
        child.__error = err;
      });

      return child;
    },

    parse(file, directory) {
      // remove all previous error listeners to avoid MaxListenersExceededWarning
      this.child.removeAllListeners('error');

      return new Promise((resolve, reject) => {
        // Reject an early error
        if (this.child.__error) return reject(this.child.__error);

        this.child
          .once('error', reject)
          .once('message', (data) => {
            resolve(JSON.parse(data));
          });

        this.child
          .send(JSON.stringify({ action: 'parse', payload: { library: name, directory, filepath: file }}))
      });
    },

    end() {
      return new Promise((resolve, reject) => {
        let handled = false;
        const handler = (errorOrCode) => {
          // both 'exit' and 'error' events could fire after an error
          if (handled) return;
          handled = true;

          if (errorOrCode instanceof Error) {
            return reject(errorOrCode);
          }

          if (!isNaN(errorOrCode) && errorOrCode !== 0) {
            return reject(new Error(`IPC parser process ended with code ${code}`))
          }

          resolve();
        };

        this.child
          .once('error', handler)
          .once('end', handler);

        this.child
          .send(JSON.stringify({ action: 'end' }))
      });
    }
  });

  return Promise.resolve(parser);
}

module.exports = pythonIPCParser;
