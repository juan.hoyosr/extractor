const { spawn } = require('child_process');
const Parser = require('./parser');

function nodeIPCParser(library) {
  const { command, args } = library;
  const child = spawn(command, args, {
    stdio: ['pipe', 'pipe', 'pipe', 'ipc'],
  });

  let error;
  // catch early spawn errors
  child.once('error', (err) => {
    error = err;
  });

  const parser = new Parser({
    parse(file, directory) {
      // remove all previous error listeners to avoid MaxListenersExceededWarning
      child.removeAllListeners('error');

      return new Promise((resolve, reject) => {
        // Reject an early error
        if (error) return reject(error);

        child
          .once('error', reject)
          .once('message', resolve);

        child
          .send({ action: 'parse', payload: { directory, filepath: file }})
      });
    },
    end() {
      return new Promise((resolve, reject) => {
        let handled = false;
        const handler = (errorOrCode) => {
          // both 'exit' and 'error' events could fire after an error
          if (handled) return;
          handled = true;

          if (errorOrCode instanceof Error) {
            return reject(errorOrCode);
          }

          if (!isNaN(errorOrCode) && errorOrCode !== 0) {
            return reject(new Error(`IPC parser process ended with code ${code}`))
          }

          resolve();
        };

        child
          .once('error', handler)
          .once('exit', handler);

        child
          .send({ action: 'end' })
      });
    }
  });

  return Promise.resolve(parser);
}

module.exports = nodeIPCParser;
