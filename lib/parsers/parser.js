
/*
* Abstract Parser
*/

// ParseError = {
//   __error__: {
//     msg: 'The error messsage',
//     filepath: 'The file that could not be saved'
//   }
// }

// ToggleArea = {
//   id: '',
//   common_id: '',
//   hash: '',
//   ast: Object,
//   start: {},
//   end: {}
// }

class Parser {
  constructor({ launch, parse, end }) {
    if (typeof launch === 'function') this.launch = launch;
    if (typeof parse === 'function') this.parse = parse;
    if (typeof end === 'function') this.end = end;

    this.__childProcess = null;
  }

  get child() {
    if (this.__childProcess) return this.__childProcess;
    this.__childProcess = this.launch();
    return this.__childProcess;
  }

  /*
  * Notice messages can be returned in any order.
  * See commit-parser.js for more info.
  *
  * @returns Array[ToggleArea|ParseError]
  */
  parse(file, directory, library) {
    throw new Error('The parse method must be implemented');
  }

  end() {
    throw new Error('The end method must be implemented');
  }
}

module.exports = Parser;
