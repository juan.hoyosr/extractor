module.exports = {
  nodejsIPCParser: require('./nodejs-ipc-parser'),
  pythonIPCParser: require('./python-ipc-parser'),
}
