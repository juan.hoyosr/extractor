const { Transform } = require('stream');
const debugLog = require('debug')('log-parser');
const minimatch = require('minimatch');

class LogParser extends Transform {
  constructor(options = {}) {
    super(Object.assign(options, { objectMode: true }));

    this.pattern = options.pattern || '';
    this._remainingChunk = '';
  }

  /*
  * TODO: avoid creating regexp on every transform.
  *
  * Here is an example of the expected log format:
  * 6863d6cac11a20d624f2f00245106ad8bf28fed5 1469829167 1517253329 8f6fa61dd6176703e2a46b97dd9e964d0f2430d7 <-- commit-hash author-timestamp committer-timestamp [parent-hash,...]\n
  * :000000 100644 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 A      api/v1/apiErrors.js
  */
  _transform(chunk, encoding, callback) {
    let strChunk = this._remainingChunk + chunk.toString('utf8');
    this._remainingChunk = ''; // reset
    // Save the last incomplete portion for the next chunk
    if (strChunk[strChunk.length - 1] !== '\n') {
      const lastLineMatch = strChunk.match(/\n(.+)$/);
      if (lastLineMatch) {
        this._remainingChunk = lastLineMatch[0];
      }
      strChunk = `${strChunk.replace(/\n.+$/, '')}\n`;
    }

    const lines = strChunk.split('\n');
    lines.forEach((line) => {
      const match = line.match(/^([0-9a-f]{40})\s([0-9]+)\s([0-9]+)\s([0-9a-f]{40})?(\s[0-9a-f]{40})?/m);
      if (match) {
        if (this._currentCommit && this._currentCommit.files.length) {
          this.push(this._currentCommit); // flush the last commit
          debugLog(`Flushed commit ${this._currentCommit.commit} (${this._currentCommit.files.length} files)`);
        }

        this._currentCommit = {
          commit: match[1],
          authorTs: match[2],
          committerTs: match[3],
          parentsCount: match.slice(4).filter(h => !!h).length,
          files: [],
        };
      } else if (line !== '') {
        const match = line.match(/([0-9]{6})\s([0-9]{6})\s[0-9a-f]{40}\s([0-9a-f]{40})\s(A|C|D|M|R|T|U|X|B)[0-9]*\s+(.+)$/m);
        if (match) {
          const [_matched, prevmod, currmod, ref, action, filepath] = match;
          if (['A', 'M', 'D'].indexOf(action) == -1) return; // only support ADD, DELETE or MODIFY

          const mods = `${prevmod}__${currmod}`;
          if (mods.indexOf('120000') > -1) return; // we don't support symlinks, skip
          if (mods.indexOf('160000') > -1) return; // skip submodules

          // git-log fails filtering filepaths. See https://gitlab.com/juan.hoyosr/extractor/issues/10
          // skip non-matching files
          if (this.pattern && !minimatch(filepath, this.pattern, { nocase: true })) return;

          this._currentCommit.files.push({
            ref,
            action,
            filepath,
          });
        } else {
          return callback(new Error(`Cannot parse the log line. Maybe the format changed? ${line}`));
        }
      }
    });

    callback();
  }

  _flush(callback) {
    if (this._currentCommit && this._currentCommit.files.length) {
      debugLog(`Flushed commit ${this._currentCommit.commit} (${this._currentCommit.files.length} files)`);
    }
    callback(null, this._currentCommit);
  }
}

module.exports = LogParser;
