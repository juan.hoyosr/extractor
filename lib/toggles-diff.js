const { Transform } = require('stream');
const { trace } = require('../git');
const debug = require('debug')('toggles-diff');
const debugType = {
  Declaration: require('debug')('toggles-diff:declaration'),
  Point: require('debug')('toggles-diff:point'),
  Router: require('debug')('toggles-diff:router'),
};

const HUNK_TO_FILE_REGEXP = /\+([0-9]+),([0-9]+) @{2,}$/m;
const ADD_FILE_DIFF_REGEXP = /\+{3,} b\/(.+)/;

class TogglesDiff extends Transform {
  constructor(options = {}) {
    options.highWaterMark = 1;
    super(Object.assign(options, { objectMode: true }));

    this.diff = {
      'Declaration': {},
      'Router': {},
      'Point': {},
    };

    this.cwd = options.cwd || '';
    this.breakOnFirst = options.breakOnFirst;
    this.traceCacheStore = {};
  }

  _transform(snapshot, encoding, callback) {
    debug('%s (%s toggles in %s files)', snapshot.commit.commit, snapshot.toggles.length, snapshot.commit.files.length);

    this.pairToggles(snapshot)
      .then((pairedToggles) => {
        pairedToggles.forEach((pair) => {
          const { previous, current } = pair;

          // Checkpoint, fail if already deleted
          if (this.alreadyDeleted(previous)) {
            throw new Error(`Cannot touch or match deleted ${current.type} ${current.id} at ${snapshot.commit.commit}.`);
          }

          if (!previous && current) {
            // Checkpoint, never overwrite
            if (this.diff[current.type][current.id]) {
              throw new Error(`Cannot overwrite operations for ${current.type} ${current.id} at ${snapshot.commit.commit}. This is likely a missing match in previous lookup.`);
            }

            // Reference the original toggle for subsequent operations
            current.original_id = current.id;
            this.diff[current.type][current.id] = [{
              commit: snapshot.commit,
              toggle: current,
              operation: 'ADDED',
            }];
            debugType[current.type]('%s (%s) ADDED', current.hash, current.id);
          } else if (previous && !current) {
            this.diff[previous.type][previous.original_id].push({
              commit: snapshot.commit,
              toggle: previous,
              operation: 'DELETED',
            });
            debugType[previous.type]('%s (%s) DELETED', previous.hash, previous.id);
          } else if (previous.hash !== current.hash) {
            // Spread the original toggle for subsequent operations
            current.original_id = previous.original_id;
            this.diff[current.type][current.original_id].push({
              commit: snapshot.commit,
              toggle: current,
              operation: 'MODIFIED',
            });
            debugType[current.type]('%s (%s) - %s (%s) MODIFIED', previous.hash, previous.id, current.hash, current.id);
          } else if (current.id !== previous.id) {
            // toggle was not explicitly changed, but it could've suffered from collateral
            // changes from other changes in the file (e.g. location of toggle in file is
            // now different).
            current.original_id = previous.original_id;
            this.diff[current.type][current.original_id].push({
              commit: snapshot.commit,
              toggle: current,
              operation: 'CONTEXT_CHANGED',
            });
            debugType[current.type]('%s (%s) CONTEXT_CHANGED', current.hash, current.id);
          } else {
            // ids are sensitive to any change (i.e. explicit/implicit) so no replacement
            // is needed. The toggle component did not changed at all
            debugType[current.type]('%s (%s) --NOCHANGE--', current.hash, current.id);
          }
        });

        // Previous cache entries are useless
        this.traceCacheStore = {};

        if (this.breakOnFirst === true && this.hasToggles()) {
          this.push(this.diff);
          this.push(null); // signal the end of the stream
          return process.nextTick(() => {
            return this.destroy();
          });
        }

        callback();
      })
      .catch(callback);
  }

  _flush(callback) {
    callback(null, this.diff);
  }

  alreadyDeleted(previous) {
    if (!previous) return false;
    const originalOps = this.diff[previous.type][previous.original_id];
    const originalOp = originalOps[originalOps.length - 1];
    return originalOp.operation === 'DELETED';
  }

  hasToggles() {
    return Object.keys(this.diff).some(type => Object.keys(this.diff[type]).length > 0)
  }

  // Returns the *last version* of all the living toggles in the system.
  getPreviousToggles(files) {
    const lastVersions = Object.values(this.diff)
      .reduce((togglesOperations, togglesInType) => {
        togglesOperations.push(...Object.values(togglesInType));
        return togglesOperations;
      }, [])
      .reduce((toggles, toggleOperations) => {
        // Living toggles only
        const lastOperation = toggleOperations[toggleOperations.length - 1];
        if (lastOperation.operation !== 'DELETED') {
          toggles.push(lastOperation.toggle);
        }
        return toggles;
      }, [])
      .reduce((togglesByFile, toggle) => {
        // Use a Set to make sure toggles are not repeated
        togglesByFile[toggle.file] = togglesByFile[toggle.file] || new Set();
        togglesByFile[toggle.file].add(toggle);
        return togglesByFile;
      }, {});

    return files
      .map(file => lastVersions[file.filepath])
      .reduce((toggles, set = []) => { // init when no toggles exist yet
        toggles.push(...set);
        return toggles;
      }, []);
  }

  async pairToggles(snapshot) {
    const pairs = [];
    const previousToggles = this.getPreviousToggles(snapshot.commit.files);
    // Run first through the previous with smallest area size (lines-wise),
    // to match inner toggles first when nested toggles.
    const currentToggles = Array.from(snapshot.toggles)
      .sort((a, b) => (a.end.line - a.start.line) - (b.end.line - b.start.line));

    // Find the previous match of every current toggle
    // IMPORTANT: All async operations must be sequenced.
    // git operations are executed against the same repository in the
    // filesystem without any concurrency control. Don't want to overlap.
    for (const current of currentToggles) {
      const previous = await this.previousLookup(
        current, previousToggles, snapshot.commit.commit
      );
      if (previous) {
        // Don't leak the previous toggle
        previousToggles.some((toggle, index, array) => {
          if (toggle.id === previous.id) {
            array.splice(index, 1);
            return true;
          }
          return false;
        });

        pairs.push({
          previous,
          current,
        });
      } else {
        // A new toggle could have the same original conditions as another toggle, thus both
        // have the same id. However, the new toggle shall be registered as a different one
        // *only* if the other toggle has not been changed.
        // This is an unlikely but possible scenario. See https://gitlab.com/juan.hoyosr/extractor/issues/3
        const toggle = this.diff[current.type][current.id];
        if (toggle && toggle.length > 1) {
          current.id = `${current.id}-${snapshot.commit.committerTs}`;
        }

        // Augment the toggle to trace it across merges
        if (snapshot.commit.parentsCount > 1) {
          Object.assign(current, await this.augmentToggle(current, snapshot));
        }

        pairs.push({
          current,
        });
      }
    }

    // Drain remaining previous toggles
    previousToggles.forEach((previous) => {
      pairs.push({
        previous,
      });
    });

    return pairs;
  }

  /*
  * Finds the last recent version of a toggle using various strategies.
  */
  async previousLookup(current, previousToggles, snapshotCommit) {
    if (this.breakOnFirst === true) return;

    const commonPrevious = previousToggles.filter(previous => previous.type === current.type);
    if (commonPrevious.length === 0) return;

    const previous = await this.lookupLineEvolution(current, commonPrevious, snapshotCommit);
    if (previous) {
      return previous;
    }

    return await this.lookupExactLatestVersion(current, commonPrevious);
  }

  /*
  * Matches a previous of a current toggle if they have the exact same
  * characteristics.
  *
  * This strategy is useful to mitigate the limitations of git-log
  * to find the origin of a line when lots of changes have occurred in
  * a file, as used in lookupLineEvolution. See issue #7.
  *
  * Notice that if this strategy is used before lookupLineEvolution the
  * history will not be the same as if after. This needs some further
  * investigation. Try with acfrmarine/squidle.
  */
  async lookupExactLatestVersion(current, previousToggles) {
    return previousToggles.find((toggle) => {
      return (
        toggle.id === current.id &&
        toggle.file === current.file &&
        toggle.start.line === current.start.line &&
        toggle.end.line === current.end.line
      );
    });
  }

  /*
  * Matches a previous of a current toggle using git-log -L to trace the
  * evolution of the lines of current and comparing the results with all
  * the versions of all existent previous toggles.
  */
  async lookupLineEvolution(current, previousToggles, snapshotCommit) {
    return await this.trace(current, snapshotCommit, (gitLogCommitHash, start, end, filepath) => {
      return previousToggles.find((toggle) => {
        const versions = Array.from(this.diff[toggle.type][toggle.original_id])
          .reverse(); // quick match

        // Check all versions, not all context changes are traced by git as we do
        return versions
          .filter(version => version.toggle.file === filepath)
          .some((version) => {
            const toggleCommit = version.commit;
            const toggleCommitHash = toggleCommit.commit;
            const toggleVersion = version.toggle;

            const samePrevious = (
              (
                toggleCommitHash === gitLogCommitHash &&
                toggleVersion.file === filepath &&
                toggleVersion.start.line >= start &&
                toggleVersion.end.line <= end
              )
              ||
              (
                // When a previous version comes in a merge because of
                // `git log --first-parent -m`, the original commit hash
                // and the area could be the traced ones
                toggleVersion._original &&
                toggleVersion._original.commitHash === gitLogCommitHash &&
                toggleVersion._original.filepath === filepath &&
                toggleVersion._original.start >= start &&
                toggleVersion._original.end <= end
              )
            );

            return samePrevious;
          });
      });
    });
  }

  /*
  * Extracts the line where a toggle was introduced from a patch
  * string of `git-log -L`.
  */
  getPreviousLine(patch) {
    let line = {
      start: NaN,
      end: NaN,
    };

    const match = patch.match(HUNK_TO_FILE_REGEXP);
    if (match) {
      const start = parseInt(match[1], 10);
      const count = parseInt(match[2], 10);
      line.start = start;
      line.end = line.start + count - 1;
    }

    return line;
  }

  trace(current, snapshotCommit, callback) {
    const { start, end, file } = current;
    const options = {
      cwd: this.cwd,
      revision: snapshotCommit,
      maxBuffer: 10 * 1024 * 1024, // expect <10MiB for each stdio buffer
      cacheStore: this.traceCacheStore
    };

    return trace(start.line, end.line, file, options)
      .then((stdout) => {
        return new Promise((resolve, reject) => {
          const patchedCommits = stdout.split(/^\n/m);
          let i = -1;
          let len = patchedCommits.length - 1;
          /*
          * Iterating through multiple commits could serve multiple purposes.
          * Two examples:
          * - the first commit is useful to augment the toggle
          * - the first commit is not be useful for line evolution lookup because
          *   it could reference the current state, not the previous
          */
          while (i < len) {
            i++;
            const patchedCommit = patchedCommits[i];
            const commitPatchLines = patchedCommit.split('\n');

            if (commitPatchLines.length < 3) {
              /*
              * You have find something that looks like a git-log bug!
              * Check this out: git log --pretty=format:%H -L 41,41:website/identifiers/clients/ezid.py 0d22d4d6591493ea9bc32d456d99836347600551
              * in https://github.com/CenterForOpenScience/osf.io
              */
              continue; // use the next commit entry, then
            }

            const gitLogCommitHash = commitPatchLines[0];
            const filepath = commitPatchLines[3].match(ADD_FILE_DIFF_REGEXP)[1];

            if (!filepath) {
              return reject(new Error(
                `No filepath found for ${current.id} at ${snapshotCommit}. ${commitPatchLines[3]}`
              ));
            }

            const { start, end } = this.getPreviousLine(patchedCommit);
            if (isNaN(start) || isNaN(end)) {
              return reject(new Error(
                `Unknown patch format for ${current.id} at ${snapshotCommit}. ${patchedCommit}`
              ));
            }

            const result = callback(...[gitLogCommitHash, start, end, filepath]);

            if (result) {
              return resolve(result);
            }
          }

          resolve();
        });
      })
      .catch((error) => {
        debug('Upcoming error while processing %s', current.id);
        throw error;
      });
  }

  async augmentToggle(toggle, snapshot) {
    const newToggle = Object.assign({}, toggle);
    const trace = await this.trace(toggle, snapshot.commit.commit, (hash, start, end, filepath) => {
      return { hash, start, end, filepath };
    });

    if (trace) {
      if (trace instanceof Error) {
        throw trace;
      } else {
        newToggle._original = {
          commitHash: trace.hash,
          filepath: trace.filepath,
          start: trace.start,
          end: trace.end,
        };
      }
    }

    return newToggle;
  }
}

module.exports = TogglesDiff;
