const { exec } = require('child_process');
const debug = require('debug')('git');

class GitCheckoutError extends Error {
  constructor(...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, GitCheckoutError);
    }

    this.name = 'GitCheckoutError';
    this.command = params[1];
  }
}

/*
* Tries to cleanup a local repository and checkout a specific reference
*
* In some situations retrying the cleanup works.
*/
function cleanup({ cwd = null, checkoutRef = '--main_branch--', retries = 0 }) {
  const MAX_RETRIES = 1;

  return new Promise((resolve, reject) => {
    const steps = [];
    const hardReset = [
      'branch=`git symbolic-ref refs/remotes/origin/HEAD | sed \'s@^refs/remotes/origin/@@\'`',
      '(git rm --cached -r . || exit 0)', // remove cached changes, but keep going if nothing is cached
      'git reset --hard $branch',
      'git clean -f -d'
    ];

    if (checkoutRef === '--main_branch--') {
      steps.push(
        ...hardReset,
        'git checkout $branch'
      );
    } else if (checkoutRef && retries === 0) {
      // This is faster, useful when moving along the history
      steps.push(
        'git clean -f -d',
        `git checkout ${checkoutRef}`
      );
    } else {
      steps.push(...[
        ...hardReset,
        `git checkout ${checkoutRef}`
      ]);
    }

    const command = `(${steps.join(' && ')}) > /dev/null`;
    exec(command, { cwd }, (error) => {
      if (error) {
        if (error.message.indexOf('overwritten') > -1) {
          if (++retries <= MAX_RETRIES) {
            return resolve(false);
          }

          // Impossible to checkout.
          // There seems to be a bug in git when you cannot move to another place because `git checkout -- file`
          // will allways show there are pending modifications. Looks related to autoctrl and lineendings.
          // https://stackoverflow.com/a/2016426/638425
          error = new GitCheckoutError('Local changes will be overritten. Cannot checkout.', command);
        }
        return reject(error);
      }
      resolve(true);
    });
  })
  .then((resolved) => {
    if (resolved) return Promise.resolve();
    const args = Object.assign({}, arguments[0]);
    args.retries = retries;
    return cleanup(args);
  });
}

function trace(start, end, file, { cwd, revision, maxBuffer, cacheStore }) {
  return new Promise((resolve, reject) => {
    const command = `git log --pretty=format:%H -L ${start},${end}:${file} ${revision}`;

    if (cacheStore[command]) {
      debug(`(cached) Tracing with ${command}`);
      return resolve(cacheStore[command]);
    }
    debug(`Tracing with ${command}`);

    exec(command, { cwd, maxBuffer }, (error, stdout, stderr) => {
      if (error) {
        // Ignore shorter file errors
        if (/fatal: file .+ has only [0-9]+ lines/.test(error.message)) {
          return resolve();
        }
        return reject(error);
      }

      if (stderr) {
        error = new Error(stderr)
        error.stderr = stderr;
        return reject(error);
      }

      cacheStore[command] = stdout;

      resolve(stdout);
    });
  });
}

module.exports = {
  cleanup,
  trace,
};
