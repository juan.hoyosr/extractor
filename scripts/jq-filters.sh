# jq filters for extractor output

# Undefined routers
jq -C '.Router."undefined-Router"[] | { commit: .commit.commit, operation: .operation, toggle: { file: .toggle.file, start_line: .toggle.start.line, hash: .toggle.hash } }' history.json | less -R

# All Routers in a simplified format
jq -C '.Router | to_entries | map(.value) | flatten | .[] | { commit: .commit.commit, operation: .operation, toggle: { id: .toggle.id, common_id: .toggle.common_id, file: .toggle.file, start_line: .toggle.start.line, hash: .toggle.hash } }' history-test.json | less -R

# All Routers with given common_id
jq -C '.Router | to_entries | map(.value) | flatten | .[] | select(.toggle.common_id | test("undefined-Router")) | { commit: .commit.commit, operation: .operation, toggle: { id: .toggle.id, common_id: .toggle.common_id, file: .toggle.file, start_line: .toggle.start.line, hash: .toggle.hash } }' history-test.json | less -R

# A specific Declaration
jq -C '.Declaration."disableHttp"[] | { commit: .commit.commit, operation: .operation, toggle: { file: .toggle.file, start_line: .toggle.start.line, hash: .toggle.hash } }' history.json | less -R

# All history
jq -C '. | to_entries | map(.value) | .[] | to_entries | map(.value) | flatten | .[] | { commit: .commit.commit, operation: .operation, toggle: { id: .toggle.id, type: .toggle.type, common_id: .toggle.common_id, file: .toggle.file, start_line: .toggle.start.line, hash: .toggle.hash } }' history-test.json | less -R

# All toggles in an array
jq -C '.[] | { id: .id, type: .type, common_id: .common_id, file: .file, start_line: .start.line, hash: .hash }' toggles.json | less -R

# All Routers
jq -C '.[] | select(.type == "Router") | { id: .id, type: .type, common_id: .common_id, file: .file, start_line: .start.line, hash: .hash }' toggles.json | less -R

# ADDED Routers: id, file, start-line
jq -C '.Router | to_entries | map(.value) | flatten | map(select(.operation == "ADDED")) | sort_by(.toggle.id) | .[] | [.toggle.id, .toggle.file, .toggle.start.line]' history.json | less -R

# Summary with similar structure
jq -C '.Router | to_entries | map({ oid: .key, ops: .value | map({ commit: .commit.commit, operation: .operation, toggle: { id: .toggle.id, file: .toggle.file, start: .toggle.start.line, end: .toggle.end.line, hash: .toggle.hash } } ) })' history.json | less -R