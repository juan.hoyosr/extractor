/*
* Traverse all commits and diff toggle areas
* 1. For every commit c1 in git-log, older first
* 2. parseProject of files in c1
* 3. parseProject of files in c1.parent
* 4. Diff ToggleAreas between 2 & 3 and store changeset
*/

const { spawn } = require('child_process');
const pump = require('pump');
const { LogParser, CommitParser, TogglesDiff } = require('./lib');
const parseProject = require('./project-parser');
const debug = require('debug')('history');
const { Writable, Readable } = require('stream');
const git = require('./git');

function parseProjectHistory(pattern, { cwd, parser, breakOnFirst, from }, callback) {
  debug('Parsing %s', pattern);
  const command = 'git';
  const args = [
    'log',
    '--date-order',
    '--raw',
    breakOnFirst === true ? '' : '--reverse', // break quickly
    '--no-abbrev',
    '--pretty=format:%H %at %ct %P',
    '--first-parent',
    '-m',
    '--no-renames',
    from ? `${from}^..HEAD` : '', // start searching from this commit
  ]
  .filter(arg => !!arg);

  debug('%s %s', command, args.join(' '));
  const gitLog = spawn(command, args, { cwd });
  gitLog.stderr.once('data', (data) => {
    process.stderr.write(data.toString());
  });

  let stdout = '';
  gitLog.stdout.on('data', (data) => {
    stdout += data.toString('utf-8');
  });

  gitLog.once('close', (code) => {
    if (code !== 0) {
      return callback(new Error(`Process exited with process ${code}`));
    }

    if (!stdout) {
      debug('No git-log entries were found with the supplied arguments');
    }

    const readable = new Readable()
    readable._read = () => {};
    readable.push(Buffer.from(stdout, 'utf-8'));
    readable.push(null);

    let history;
    let totalFiles = !!stdout ? stdout.match(/^:/mg, '').length : 0;
    pump(
      readable,
      new LogParser({ pattern }),
      new CommitParser({ projectParser: parseProject, cwd, parser, breakOnFirst, totalFiles }),
      new TogglesDiff({ cwd, breakOnFirst }),
      new Writable({
        objectMode: true,
        write(diff, encoding, callback) {
          history = diff;
          callback();
        },
      }),
      (error) => {
        if (error) return callback(error);
        callback(null, history);
      }
    );
  });
}

module.exports = (filepath, options) => {
  const { cwd } = options;
  debug('Checking out the main branch of the repository...');
  return git.cleanup({ cwd })
    .then(() => {
      return new Promise((resolve, reject) => {
        parseProjectHistory(filepath, options, (err, toggles) => {
          if (err) return reject(err);
          resolve(toggles);
        });
      });
    });
};
