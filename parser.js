const parseProject = require('./project-parser');
const parseProjectHistory = require('./history-parser');
const parserFactory = require('./lib/parser-factory');

async function parser(filepath, options) {
  const { library, history } = options;
  const parser = await parserFactory.build(library);
  const parserOptions = Object.assign({ parser }, options);
  let toggles;

  if (!history) {
    toggles = await parseProject(filepath, parserOptions);
  } else {
    toggles = await parseProjectHistory(filepath, parserOptions);
  }

  parser.end();
  return toggles;
}

module.exports = parser;
