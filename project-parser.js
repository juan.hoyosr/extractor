const _glob = require('glob');
const debug = require('debug')('project-parser')

const glob = (files, options) => {
  return new Promise((resolve, reject) => {
    _glob(files, options, (err, files) => {
      if (err) return reject(err);
      resolve(files);
    });
  });
};

async function parseProject(filesPattern, { cwd, parser, breakOnFirst, commit }) {
  const options = {
    // TODO: decouple ignore options from node.js projects
    ignore: '**/node_modules/**',
    cwd,
  };

  const files = await glob(filesPattern, options);
  const togglesSets = [];
  // for...or the files and await the parse response.
  // IMPORTANT: All async operations must be sequenced.
  // git operations are executed against the same repository in the
  // filesystem without any concurrency control. Don't want to overlap.
  for (const file of files) {
    debug(`Parsing ${file} at ${commit ? commit.commit : 'HEAD'}`)
    const toggles = await parser.parse(file, cwd);
    togglesSets.push(...toggles);
    if (breakOnFirst === true && toggles.filter(t => !t.__error__).length > 0) break;
  }
  return togglesSets;
}

module.exports = parseProject;