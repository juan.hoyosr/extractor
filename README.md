# extractor

Extract toggles components from the source code of a project and prints them to stdout.

## Installation

```sh
$ npm install
```

## Test

```sh
$ npm test
```

## Supported libraries

At this moment only Django-Waffle using the [extractor-python](https://gitlab.com/juan.hoyosr/extractor-python) parser is fully supported.

The [extractor-js](https://gitlab.com/juan.hoyosr/extractor-js) parser can also be used to extract from [refocus](https://github.com/salesforce/refocus) library, but it needs further testing and could not even work at all because the development focused to integrate with extractor-pyton.

## Examples

Extract [Django-Waffle](https://github.com/django-waffle/django-waffle) toggles using a Python extractor.

```bash
time PYTHON_PATH=`pyenv which python` SCRIPT_PATH=~/projects/extractor-python extractor "~/repositories/course-discovery" "*.py" --history django-waffle > course-discovery.json

# real	8m9.109s
# user	9m52.412s
# sys	1m34.315s
```

Print the toggles of a cloned copy of [refocus](https://github.com/salesforce/refocus) using [feature-toggles](https://github.com/alexlawrence/feature-toggles) traces.

```bash
time extractor "~/repositories/refocus" "config/toggles.js" feature-toggles

# real	0m0.469s
# user	0m0.374s
# sys	0m0.070s
```

Extract the toggles across all the history of a project using a specific toggling library and pretty prints them through [jq](https://stedolan.github.io/jq/).

```bash
time extractor --history "~/repositories/refocus" "*.js" feature-toggles | jq -C '.' | less -R

# real	2m19.301s
# user	2m56.515s
# sys	0m52.461s
```

## Install and use

Creating an npm link will work to make the command available in your shell:

```sh
$ npm link
$ extractor --help
```
