const LogParser = require('../lib/log-parser');

describe('LogParser', () => {
  it('returns a commit entry', (done) => {
    const entry = [
      '6863d6cac11a20d624f2f00245106ad8bf28fed5 1469829167 1517253329 \n',
      ':000000 100644 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 A      api/v1/apiErrors.js\n',
    ].join('');

    const parser = new LogParser();

    parser.on('data', (commit) => {
      expect(commit).to.deep.equal({
        commit: '6863d6cac11a20d624f2f00245106ad8bf28fed5',
        authorTs: '1469829167',
        committerTs: '1517253329',
        files: [{
          ref: 'e1af647a8369ed7897a714a13ddff0c6ece27593',
          action: 'A',
          filepath: 'api/v1/apiErrors.js',
        }],
        parentsCount: 0,
      });

      done();
    });

    parser.once('error', done);

    parser.write(entry);
    parser.end();
  });

  it('discards symlinks or submodules', (done) => {
    const entry = [
      '6863d6cac11a20d624f2f00245106ad8bf28fed5 1469829167 1517253329 9f43e82c1698dc90ddab245da4a6897faa4578b7\n',
      ':000000 120000 0000000000000000000000000000000000000000 ef08f31f97e7d3d4b2018293bb22c5d9389e6a53 A      a_symlink.js\n',
      ':000000 100644 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 A      api/v1/apiErrors.js\n',
      ':120000 000000 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 D      another_symlink.js\n',
      ':160000 000000 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 D      a_submodule\n',
    ].join('');

    const parser = new LogParser();

    const commits = [];
    parser.on('data', (commit) => {
      commits.push(commit);
    });

    parser.once('error', done);

    parser.once('end', () => {
      expect(commits[0]).to.deep.equal({
        commit: '6863d6cac11a20d624f2f00245106ad8bf28fed5',
        authorTs: '1469829167',
        committerTs: '1517253329',
        files: [{
          ref: 'e1af647a8369ed7897a714a13ddff0c6ece27593',
          action: 'A',
          filepath: 'api/v1/apiErrors.js',
        }],
        parentsCount: 1,
      });

      done();
    })

    parser.write(entry);
    parser.end();
  });

  it('discards files not matching the filepath pattern', (done) => {
    const entry = [
      '6863d6cac11a20d624f2f00245106ad8bf28fed5 1469829167 1517253329 9f43e82c1698dc90ddab245da4a6897faa4578b7\n',
      ':000000 100644 0000000000000000000000000000000000000000 ef08f31f97e7d3d4b2018293bb22c5d9389e6a53 A      main.js\n',
      ':000000 100644 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 A      api/v1/apiErrors.py\n',
      ':000000 100644 0000000000000000000000000000000000000000 3a72420f38c2b41019aa622d612d56c0b0b6c615 A      errors.js\n',
      ':000000 100644 0000000000000000000000000000000000000000 dcfe058e8af9b10bff10b1e7dd2a9ac080255d70 A      errors.html\n',
    ].join('');

    const parser = new LogParser({ pattern: '**/*.{py,html}' });

    const commits = [];
    parser.on('data', (commit) => {
      commits.push(commit);
    });

    parser.once('error', done);

    parser.once('end', () => {
      expect(commits).to.have.length(1);
      const { files } = commits[0];
      expect(files).to.deep.equal([
        {
          ref: 'e1af647a8369ed7897a714a13ddff0c6ece27593',
          action: 'A',
          filepath: 'api/v1/apiErrors.py',
        },
        {
          ref: 'dcfe058e8af9b10bff10b1e7dd2a9ac080255d70',
          action: 'A',
          filepath: 'errors.html',
        },
      ]);

      done();
    })

    parser.write(entry);
    parser.end();
  });

  it('dicards commits without files', (done) => {
    const entry = [
      'e1af647a8369ed7897a714a13ddff0c6ece27593 1469829167 1517253329 9f43e82c1698dc90ddab245da4a6897faa4578b7\n',
      ':000000 120000 0000000000000000000000000000000000000000 ef08f31f97e7d3d4b2018293bb22c5d9389e6a53 A      a_symlink.js\n',
      '\n',
      '6863d6cac11a20d624f2f00245106ad8bf28fed5 1469829167 1517253329 \n',
      ':000000 100644 0000000000000000000000000000000000000000 e1af647a8369ed7897a714a13ddff0c6ece27593 A      api/v1/apiErrors.js\n',
    ].join('');

    const parser = new LogParser();

    const commits = [];
    parser.on('data', (commit) => {
      commits.push(commit);
    });

    parser.once('error', done);

    parser.once('end', () => {
      expect(commits).to.deep.equal([
        {
          commit: '6863d6cac11a20d624f2f00245106ad8bf28fed5',
          authorTs: '1469829167',
          committerTs: '1517253329',
          files: [{
            ref: 'e1af647a8369ed7897a714a13ddff0c6ece27593',
            action: 'A',
            filepath: 'api/v1/apiErrors.js',
          }],
          parentsCount: 0,
        }
      ]);

      done();
    })

    parser.write(entry);
    parser.end();
  });
});
