describe('CommitParser', () => {
  it('parses the files as if a project', (done) => {
    const CommitParser = require('proxyquire')('../lib/commit-parser', {
      '../git': {
        cleanup: () => Promise.resolve(),
      }
    });

    const toggles = ['toggles!'];
    const commit = {
      files: [
        {
          ref: 'foo',
          action: 'A',
          filepath: 'foo.js',
        },
        {
          ref: 'bar',
          action: 'A',
          filepath: 'api/bar.js',
        }
      ],
    };

    projectParser = sinon.stub().resolves(toggles)
    const parser = new CommitParser({ projectParser });

    parser.on('data', (data) => {
      expect(data).to.deep.equal({
        commit,
        toggles,
      });

      done();
    });

    parser.once('error', done);

    parser.write(commit);
  });

  it('does not emit a file if there was an error parsing it', (done) => {
    const CommitParser = require('proxyquire')('../lib/commit-parser', {
      '../git': {
        cleanup: () => Promise.resolve(),
      }
    });

    const commit = {
      files: [
        {
          ref: 'foo',
          action: 'A',
          filepath: 'foo.js',
        },
        {
          ref: 'bar',
          action: 'A',
          filepath: 'api/bar.js',
        }
      ],
    };

    const stub = sinon.stub().resolves([
      'a-toggle-in-foo.js',
      {
        "__error__": {
          msg: 'Cannot parse the file',
          filepath: 'api/bar.js',
        }
      }
    ]);

    const parser = new CommitParser({ projectParser: stub });

    let out;
    parser.on('data', (data) => {
      out = data;
    });
    parser.once('error', done);
    parser.once('end', () => {
      expect(out).to.deep.equal({
        commit: {
          files: [
            {
              ref: 'foo',
              action: 'A',
              filepath: 'foo.js',
            },
          ],
        },
        toggles: ['a-toggle-in-foo.js'],
      });

      done();
    });

    parser.end(commit);
  });

  it('does not send deleted files to the project parser', (done) => {
    const CommitParser = require('proxyquire')('../lib/commit-parser', {
      '../git': {
        cleanup: () => Promise.resolve(),
      }
    });

    const toggles = ['toggles!'];
    const commit = {
      files: [
        {
          ref: 'foo',
          action: 'A',
          filepath: 'foo.js',
        },
        {
          ref: 'bar',
          action: 'D',
          filepath: 'api/bar.js',
        }
      ],
    };

    projectParser = sinon.stub().resolves(toggles)
    const parser = new CommitParser({ projectParser });

    parser.on('data', (data) => {
      expect(projectParser).to.have.been.calledOnceWith('foo.js');
      done();
    });

    parser.once('error', done);

    parser.write(commit);
  });
});
