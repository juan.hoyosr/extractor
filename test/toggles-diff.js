const proxyquire = require('proxyquire');

const diffBuilder = (commit, filepath = 'foo.js', hunkRange) => `
    >>>> COMMIT HASH <<<<
    diff --git a/javascript.js b/javascript.js
    --- /dev/null
    +++ b/>>>> FILEPATH <<<<
    >>>> HUNK RANGE <<<<
    +if (isEnabled('toggle-A') || somethingElse()) {
    +  // bar
  `.replace(/\n\s+/g, '\n').substring(1) // cleanup
  .replace('>>>> COMMIT HASH <<<<', commit)
  .replace('>>>> FILEPATH <<<<', filepath)
  .replace('>>>> HUNK RANGE <<<<', hunkRange);

const diffSnapshots = (snapshots, gitLogs = [], traceFn) => {
  return new Promise((resolve, reject) => {
    snapshots = [].concat(snapshots);

    const getStdout = (gitLog) => [].concat(gitLog)
      .map(({ commit, filepath, hunkRange }) => diffBuilder(commit, filepath, hunkRange))
      .join('\n');

    const trace = traceFn || (() => {
      if (typeof gitLogs !== 'string') {
        const gitLog = gitLogs.shift();
        if (gitLog) {
          stdout = getStdout(gitLog);
        }
      } else {
        stdout = gitLogs;
      }
      return Promise.resolve(stdout);
    });

    const TogglesDiff = proxyquire('../lib/toggles-diff', {
      '../git': { trace },
    });
    const togglesDiff = new TogglesDiff();
    togglesDiff.once('error', reject);
    togglesDiff.once('data', (diff) => resolve(diff));
    snapshots.forEach((snapshot) => togglesDiff.write(snapshot));
    togglesDiff.end();
  });
};

describe('TogglesDiff - simple cases', () => {
  it('traces when a toggle component was added', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-id',
        hash: 'toggle-hash-1',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 1, column: 0 },
        end: { line: 1, column: 10 },
      }],
    };

    const { Declaration: diff } = await diffSnapshots(snapshot_t1);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-id');
    expect(diff).to.deep.equal({
      'toggle-id': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ]
    });
  });

  it('traces when a toggle component was modified', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-A',
        common_id: 'same-toggle',
        hash: 'toggle-hash-1',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 1, column: 0 },
        end: { line: 1, column: 10 },
      }],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-B',
        common_id: 'same-toggle',
        hash: 'toggle-hash-2',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 1, column: 0 },
        end: { line: 1, column: 10 },
      }],
    };

    const gitLogs = [
      // snapshot_t1
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +1,2 @@',
      },

      // snapshot_t2
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +1,2 @@',
      },
    ];

    const { Declaration: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-A');
    expect(diff).to.deep.equal({
      'toggle-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        }
      ]
    });
  });

  it('traces when a toggle component was deleted', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }, { filepath: 'bar.js' }],
      },
      toggles: [
        {
          id: 'toggle-in-foo',
          hash: 'toggle-hash-1',
          type: 'Declaration',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'toggle-in-bar',
          hash: 'toggle-hash-2',
          type: 'Declaration',
          file: 'bar.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [],
    }

    const { Declaration: diff } = await diffSnapshots([snapshot_t1, snapshot_t2]);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-in-foo', 'toggle-in-bar');
    expect(diff).to.deep.equal({
      'toggle-in-foo': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'DELETED',
        }
      ],

      'toggle-in-bar': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[1],
          operation: 'ADDED',
        }
      ]
    });
  });

  it('traces when a toggle component has not changed', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const gitLogs = [
      // snapshot_t2
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ],
    });
  });

  it('traces when a toggle is modified and previous within range of the diff hunk', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-A',
        common_id: 'same-toggle',
        hash: 'toggle-hash-1',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 2, column: 0 },
        end: { line: 2, column: 10 },
      }],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-B',
        common_id: 'same-toggle',
        hash: 'toggle-hash-2',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 3, column: 0 },
        end: { line: 3, column: 10 },
      }],
    };

    const gitLogs = [
      // snapshot_t1
      {
        commit: 'commit-hash-t1',
        // toggle-A is in the second line of the to-file hunk id
        hunkRange: '@@ -0,0 +1,2 @@',
      },

      // snapshot_t2
      [
        {
          commit: 'commit-hash-t2',
          hunkRange: '@@ -1,2 +2,2 @@',
        },
        {
          commit: 'commit-hash-t1',
          hunkRange: '@@ -0,0 +1,2 @@',
        },
      ]
    ];

    const { Declaration: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-A');
    expect(diff).to.deep.equal({
      'toggle-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        }
      ]
    });
  });

  it('traces when a multi-line toggle component was context changed', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-A',
        hash: 'toggle-hash-1',
        type: 'Point',
        file: 'foo.js',
        start: { line: 1, column: 0 },
        end: { line: 10, column: 10 },
      }],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-B',
        hash: 'toggle-hash-1',
        type: 'Point',
        file: 'foo.js',
        start: { line: 11, column: 0 },
        end: { line: 20, column: 10 },
      }],
    };

    // Mimic git log -L and return an unrelated commit
    // when doing single-line tracing where a multi-line
    // should done be instead.
    const traceFn = (start, end) => {
      if (start === end) {
        return Promise.resolve(
          diffBuilder('an-unrelated-commit', 'foo.js', '@@ -0,0 +1,2 @@')
        );
      } else {
        return Promise.resolve(
          diffBuilder('commit-hash-t1', 'foo.js', '@@ -0,0 +1,10 @@')
        );
      }
    };

    const { Point: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], null, traceFn);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-A');
    expect(diff).to.deep.equal({
      'toggle-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'CONTEXT_CHANGED',
        }
      ]
    });
  });
});

describe('TogglesDiff - when common to a group of toggles in the same file', () => {
  it('traces the addition of a toggle', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const gitLogs = [
      // snapshot_t2 - router-B
      [],

      // snapshot_t2 - router-A
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ],

      'router-B': [
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'ADDED',
        },
      ],
    });
  });

  it('traces the modification of a toggle', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 5, column: 5 },
          end: { line: 5, column: 15 },
        },
      ],
    };

    const snapshot_t3 = {
      commit: {
        commit: 'commit-hash-t3',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-D',
          common_id: 'same-router',
          hash: 'toggle-hash-3',
          type: 'Router',
          file: 'foo.js',
          start: { line: 6, column: 5 },
          end: { line: 6, column: 15 },
        },
      ],
    };

    const gitLogs = [
      // router-A at t2, git-log not tracing correctly
      {
        commit: 'commit-hash-t0',
        hunkRange: '@@ -0,0 +1,2 @@',
      },

      // router-B & C
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },

      // router-A at t3, git-log not tracing correctly
      {
        commit: 'commit-hash-t0',
        hunkRange: '@@ -0,0 +1,2 @@',
      },

      // router-B & C & D
      // no touch happened to router-B, so git will only show the
      // originating commit
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2, snapshot_t3], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
      ],

      // router-C is router-B
      'router-B': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[1],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[1],
          operation: 'CONTEXT_CHANGED',
        },
        {
          commit: snapshot_t3.commit,
          toggle: snapshot_t3.toggles[1],
          operation: 'MODIFIED',
        },
      ],
    });
  });

  it('traces the addition and modification of toggles', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-3',
          type: 'Router',
          file: 'foo.js',
          start: { line: 5, column: 5 },
          end: { line: 5, column: 15 },
        },
      ],
    };

    const gitLogs = [
      // snapshot_t2 - router-B
      [],

      // snapshot_t2 - router-A/C
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      // router-C is router-A
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[1],
          operation: 'MODIFIED',
        },
      ],

      'router-B': [
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'ADDED',
        },
      ],
    });
  });

  it('traces non-explicit modifications of a toggle', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 2, column: 0 },
          end: { line: 2, column: 10 },
        },
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 6, column: 5 },
          end: { line: 6, column: 15 },
        },
      ],
    }

    const gitLogs = [
      // snapshot_t2 - router-B
      [],

      // snapshot_t2 - router-A/C
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      // router-C is router-A
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[1],
          operation: 'CONTEXT_CHANGED',
        },
      ],

      'router-B': [
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'ADDED',
        },
      ],
    });
  });

  it('traces the deletion of a toggle', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 5, column: 0 },
          end: { line: 5, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-C', // location in file has changed
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const gitLogs = [
      // snapshot_t2 - router-C/B
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +5,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'DELETED',
        }
      ],

      // router-C is router-B
      'router-B': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[1],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'CONTEXT_CHANGED',
        }
      ]
    });
  });

  it('traces the deletion of a toggle after being modified', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 1, column: 0 },
          end: { line: 1, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const snapshot_t3 = {
      commit: {
        commit: 'commit-hash-t3',
        authorTs: 6758500,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [],
    };

    const gitLogs = [
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +1,2 @@',
      },
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +1,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots(
      [snapshot_t1, snapshot_t2, snapshot_t3],
      gitLogs
    );
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        },
        {
          commit: snapshot_t3.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'DELETED',
        }
      ],
    });
  });

  it('traces multiple modifications of a toggle', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        }
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-2',
          type: 'Router',
          file: 'foo.js',
          start: { line: 20, column: 0 },
          end: { line: 20, column: 10 },
        },
      ],
    };

    const snapshot_t3 = {
      commit: {
        commit: 'commit-hash-t3',
        authorTs: 6758500,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-3',
          type: 'Router',
          file: 'foo.js',
          start: { line: 20, column: 0 },
          end: { line: 20, column: 10 },
        },
      ],
    };

    const gitLogs = [
      // router-B
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },

      // router-C
      [
        {
          commit: 'commit-hash-t2',
          hunkRange: '@@ -2,1 +19,2 @@',
        },
        // commit-hash-t1, will not be used by the previous lookup,
        // but I leave it here to be more consistent with reality
        {
          commit: 'commit-hash-t1',
          hunkRange: '@@ -0,0 +4,2 @@',
        },
      ],
    ];

    const { Router: diff } = await diffSnapshots(
      [snapshot_t1, snapshot_t2, snapshot_t3],
      gitLogs
    );
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        },
        {
          commit: snapshot_t3.commit,
          toggle: snapshot_t3.toggles[0],
          operation: 'MODIFIED',
        },
      ],
    });
  });

  it('traces two different toggles with the same id but different addition times', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        committerTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        committerTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 6, column: 0 },
          end: { line: 6, column: 10 },
        },
      ],
    };

    const snapshot_t3 = {
      commit: {
        commit: 'commit-hash-t3',
        committerTs: 7758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 6, column: 0 },
          end: { line: 6, column: 10 },
        },
        {
          id: 'router-A', // aka router-A'
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const gitLogs = [
      // snapshot_t2 - router-A/B
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },

      // snapshot_t3 - router-A/B
      {
        commit: 'commit-hash-t1',
        hunkRange: '@@ -0,0 +4,2 @@',
      },

      // snapshot_t3 - router-A'
      {
        commit: 'commit-hash-t3', // whatever is is line 4 at this commit
        hunkRange: '@@ -0,0 +4,2 @@',
      },
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2, snapshot_t3], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-A-7758499');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'CONTEXT_CHANGED',
        },
      ],

      'router-A-7758499': [
        {
          commit: snapshot_t3.commit,
          toggle: snapshot_t3.toggles[1],
          operation: 'ADDED',
        },
      ],
    });
  });

  it('traces nested toggles', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        committerTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'point-A',
          hash: 'point-A-hash-1',
          type: 'Point',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 10, column: 10 },
        },
        {
          id: 'point-B',
          hash: 'point-B-hash-1',
          type: 'Point',
          file: 'foo.js',
          start: { line: 6, column: 0 },
          end: { line: 8, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        committerTs: 6758499,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'point-C',
          hash: 'point-A-hash-2',
          type: 'Point',
          file: 'foo.js',
          start: { line: 5, column: 0 },
          end: { line: 15, column: 10 },
        },
        {
          id: 'point-B',
          hash: 'point-B-hash-1',
          type: 'Point',
          file: 'foo.js',
          start: { line: 6, column: 0 },
          end: { line: 8, column: 10 },
        },
      ],
    };

    const traceFn = (start) => {
      if (start === 5) {
        // point A/C
        return Promise.resolve(
          diffBuilder('commit-hash-t1', 'foo.js', '@@ -0,0 +4,7 @@')
        );
      } else {
        // point B/D
        return Promise.resolve(
          diffBuilder('commit-hash-t1', 'foo.js', '@@ -0,0 +6,3 @@')
        );
      }
    };

    const { Point: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], null, traceFn);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('point-A', 'point-B');
    expect(diff).to.deep.equal({
      'point-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        },
      ],

      'point-B': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[1],
          operation: 'ADDED',
        },
      ],
    });
  });
});

describe('TogglesDiff - when common to a group of toggles in different files', () => {
  it('traces the addition of toggles', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'foo.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'bar.js' }],
      },
      toggles: [
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'bar.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2]);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ],

      'router-B': [
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'ADDED',
        },
      ],
    });
  });

  it('traces different toggles from different files', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'baz.js' }, { filepath: 'bar.js' }],
      },
      toggles: [
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'baz.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
        {
          id: 'router-B',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'bar.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'commit-hash-t2',
        authorTs: 6758499,
        files: [{ filepath: 'baz.js' }, { filepath: 'bar.js' }],
      },
      toggles: [
        {
          id: 'router-C',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'bar.js',
          start: { line: 5, column: 0 },
          end: { line: 5, column: 10 },
        },
        {
          id: 'router-A',
          common_id: 'same-router',
          hash: 'toggle-hash-1',
          type: 'Router',
          file: 'baz.js',
          start: { line: 4, column: 0 },
          end: { line: 4, column: 10 },
        },
      ],
    };

    const gitLog = [
      // router-B/C
      {
        commit: 'commit-hash-t1',
        filepath: 'bar.js',
        hunkRange: '@@ -0,0 +4,1 @@',
      },

      // router-A
      {
        commit: 'commit-hash-t1',
        filepath: 'baz.js',
        hunkRange: '@@ -0,0 +4,1 @@',
      }
    ];

    const { Router: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLog);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('router-A', 'router-B');
    expect(diff).to.deep.equal({
      'router-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ],

      'router-B': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[1],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'CONTEXT_CHANGED',
        },
      ],
    });
  });
});

describe('TogglesDiff - the unexpected', () => {
  it('traces an addition even if the git log shows commits without diff patches', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'commit-hash-t1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
      },
      toggles: [{
        id: 'toggle-id',
        hash: 'toggle-hash-1',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 1, column: 0 },
        end: { line: 1, column: 10 },
      }],
    };

    const gitLog = [
      'commit-hash-t1',
      'diff --git a/javascript.js b/javascript.js',
      '--- /dev/null',
      '+++ b/javascript.js',
      '@@ -0,0 +4,2 @@',
      '+if (isEnabled(\'toggle-A\') || somethingElse()) {',
      '+  // bar',
      '\nsome-weird-commit-hash\n',
    ].join('\n');

    const { Declaration: diff } = await diffSnapshots(snapshot_t1, gitLog);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-id');
    expect(diff).to.deep.equal({
      'toggle-id': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        }
      ]
    });
  });

  it('traces the same toggle even if coming from merges', async () => {
    const snapshot_t1 = {
      commit: {
        commit: 'merge-commit-hash-1',
        authorTs: 123456,
        files: [{ filepath: 'foo.js' }],
        parentsCount: 2,
      },
      toggles: [{
        id: 'toggle-A',
        common_id: 'same-toggle',
        hash: 'toggle-hash-1',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 20, column: 0 },
        end: { line: 20, column: 10 },
      }],
    };

    const snapshot_t2 = {
      commit: {
        commit: 'merge-commit-hash-2',
        authorTs: 6758499,
        files: [{ filepath: 'foo.js' }],
        parentsCount: 2,
      },
      toggles: [{
        id: 'toggle-B',
        common_id: 'same-toggle',
        hash: 'toggle-hash-2',
        type: 'Declaration',
        file: 'foo.js',
        start: { line: 50, column: 0 },
        end: { line: 50, column: 10 },
      }],
    };

    const gitLogs = [
      // snapshot_t1 - augment call
      {
        commit: 'inner-commit',
        hunkRange: '@@ -0,0 +1,2 @@',
      },

      // snapshot_t2 - toggle-B
      {
        commit: 'inner-commit',
        hunkRange: '@@ -0,0 +1,2 @@',
      },
    ];

    const { Declaration: diff } = await diffSnapshots([snapshot_t1, snapshot_t2], gitLogs);
    expect(diff).to.exist;
    expect(diff).to.have.all.keys('toggle-A');
    expect(diff).to.deep.equal({
      'toggle-A': [
        {
          commit: snapshot_t1.commit,
          toggle: snapshot_t1.toggles[0],
          operation: 'ADDED',
        },
        {
          commit: snapshot_t2.commit,
          toggle: snapshot_t2.toggles[0],
          operation: 'MODIFIED',
        }
      ]
    });
  });
});
